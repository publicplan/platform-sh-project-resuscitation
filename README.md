# platform.sh project resuscitation

This script allows you to backup your database dump and a specified mount.


## Installation

OS X & Linux:

```sh
git clone bitbucket.org:publicplan/platform-sh-project-resuscitation.git
```

Copy configuration file
```sh
cp platform-sh-project-resuscitation/project-resuscitation.config.dist platform-sh-project-resuscitation/project-resuscitation.config
```

## Requirements
* [platform.sh CLI](https://docs.platform.sh/gettingstarted/cli.html)
* [PHP](https://www.php.net/manual/en/install.php) 7.0+

## Usage example (Cron)

Open crontab
```sh
crontab -e
```
You can run this script in a cron. Like that:
```sh
PATH="$HOME/.platformsh/bin:$PATH"
0 0 * * * PATH-TO-PROJECT/platform-sh-project-resuscitation/backup.sh >> PATH-TO-PROJECT/my_log.log 2> PATH-TO-PROJECT/my_log.log.error.log
```