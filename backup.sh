#!/bin/bash

# Current script dir
BASEDIR=$(dirname "$0")

if [[ -f "$BASEDIR/project-resuscitation.config" ]];
then
  source "$BASEDIR/project-resuscitation.config"
else
  source "$BASEDIR/project-resuscitation.config.dist"
fi

# Update platform console to prevent update messages
platform self:update -y

# Timestamp for current backup
TIMESTAMP=$(date +"%s")

echo ""
echo "Running backups..."
echo $(date)

# Iterate all platform projects that prefixed by $CONFIG_PROJECT_NAME_PREFIX
for PROJECT_ID in $(platform project:list --no-header --title="${CONFIG_PROJECT_NAME_PREFIX}" --pipe);
do
  # Get projects
  PROJECT_LINE=$(platform project:list --no-header | grep -i ".*${PROJECT_ID}")

  # Regular expression for id and name matching
  REGEX="\| [a-z0-9]+ \| ([a-zA-Z0-9_]*).*"
  # Execute regular expression (saves result to BASH_REMATCH)
  [[ "${PROJECT_LINE}" =~ $REGEX ]]
  PROJECT_NAME="${BASH_REMATCH[1]}"

  # Create dump folder
  DUMP_BASE_PATH="${BASEDIR}/dumps/${PROJECT_NAME}/${PROJECT_ID}_${PROJECT_NAME}_${TIMESTAMP}"
  DUMP_PATH="${DUMP_BASE_PATH}/${PROJECT_ID}_${PROJECT_NAME}_${TIMESTAMP}"
  mkdir -p $DUMP_PATH

  # Download database
  if [[ $CONFIG_DOWNLOAD_DATABASE == 1 ]];
  then
    echo "Downloading files from project $PROJECT_NAME($PROJECT_ID)."
    platform mount:download -e ${CONFIG_PROJECT_ENVIRONMENT} -p $PROJECT_ID -m $CONFIG_PROJECT_SHARED_FOLDER --target $DUMP_PATH -y > /dev/null
    echo "Download finished."
  fi

  # Download files
  if [[ $CONFIG_DOWNLOAD_FILES == 1 ]];
  then
    echo "Downloading database from project $PROJECT_NAME($PROJECT_ID)."
    platform db:dump -e ${CONFIG_PROJECT_ENVIRONMENT} -p $PROJECT_ID -y --gzip -f "$DUMP_PATH/database-${CONFIG_PROJECT_ENVIRONMENT}-$PROJECT_NAME-$PROJECT_ID-${CONFIG_PROJECT_ENVIRONMENT}.sql.gz"
    echo "Dump downloaded."
  fi

  # Compressing
  echo "Compressing folder..."
  tar -zcf "$DUMP_PATH.tar.gz" "$DUMP_PATH"
  echo "Compressing finished."

  # Clean up
  echo "Removing uncompressed files..."
  rm -rf $DUMP_PATH
  echo "Removed uncompressed files."
  echo "Remove old dumps. (keeping ${CONFIG_KEEP_BACKUP_COUNT} dumps)"
  OLD_FILES=$(ls $DUMP_BASE_PATH -r | tail +$CONFIG_KEEP_BACKUP_COUNT)
  if [[ ! -z $OLD_FILES ]]; then
    rm $OLD_FILES
  fi
done
echo "Finished backups."
echo $(date)
